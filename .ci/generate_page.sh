#!/bin/sh -ex
# Copyright 2021-2022 Oliver Smith
# Copyright 2022 Dylan Van Assche
# SPDX-License-Identifier: GPL-3.0-or-later

export CHANNELS="
	v24.06-edge
	"
export DEVICES="
	qemu-amd64
	"
export UIS="
	phosh
	plasma-mobile
	sxmo-de-dwm
	sxmo-de-sway
	console
	"

mkdir -p public

passed=1

for CHANNEL in $CHANNELS; do
	for DEVICE in $DEVICES; do
		for UI in $UIS; do
                        echo "Checking \"$CHANNEL\" for device \"$DEVICE\" and UI \"$UI\""
			if ! [ -e upgrade-compat-"$CHANNEL"-"$DEVICE"-"$UI".check-passed ]; then
				passed=0
				break
			fi
		done
	done
done

if [ "$passed" -eq 1 ]; then
	cp badges/upgrade-compat-passed.svg public/upgrade-compat.svg
else
	cp badges/upgrade-compat-failed.svg public/upgrade-compat.svg
fi
